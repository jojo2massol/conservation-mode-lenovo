# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-10 21:36+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ideapad-mode@annexhack.inceptive.ru/extension.js:134
msgid "Conservation Mode"
msgstr ""

#: ideapad-mode@annexhack.inceptive.ru/extension.js:148
msgid "Fn Lock"
msgstr ""

#: ideapad-mode@annexhack.inceptive.ru/extension.js:161
msgid "Camera Power"
msgstr ""

#: ideapad-mode@annexhack.inceptive.ru/extension.js:174
msgid "USB Charging"
msgstr ""

#: ideapad-mode@annexhack.inceptive.ru/extension.js:186
msgid "Touchpad"
msgstr ""

#: ideapad-mode@annexhack.inceptive.ru/extension.js:222
msgid "Conservation Mode ERR"
msgstr ""

#: ideapad-mode@annexhack.inceptive.ru/extension.js:267
msgid "Battery conservation mode not available."
msgstr ""
